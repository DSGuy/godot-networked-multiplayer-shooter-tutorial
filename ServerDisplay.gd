# Code written by Oladeji Sanyaolu (ServerDisplay) 17/2/2022

extends Label

var ip_address : String = ""

onready var join_button : Button = $Join_Button

func _ready() -> void:
	add_to_group("ServerDisplay")
	join_button.connect("pressed", self, "_on_Join_Button_pressed")

func _on_Join_Button_pressed() -> void:
	Network.ip_address = ip_address
	Network.join_server()
	
	get_parent().get_parent().queue_free()
