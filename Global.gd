# Code written by Oladeji Sanyaolu (Global) 17/2/2022

extends Node

var player_master = null
var ui = null

var alive_players : Array = []

func instance_node_at_location(node: Object, parent : Object, location : Vector2) -> Object:
	var node_instance : Object = instance_node(node, parent)
	node_instance.global_position = location
	return node_instance

func instance_node(node : Object, parent : Object) -> Object:
	var node_instance : Object = node.instance()
	parent.add_child(node_instance)
	return node_instance
