# Code written by Oladeji Sanyaolu (SimplePrompt) 17/2/2022

extends Control

onready var okay = $Panel/Okay

func _ready() -> void:
	okay.connect("pressed", self, "_on_Okay_pressed")

func set_text(text) -> void:
	$Label.text = text

func _on_Okay_pressed() -> void:
	get_tree().change_scene("res://NetworkSetup.tscn")
	pass
