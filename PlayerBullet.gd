# Code written by Oladeji Sanyaolu (PlayerBullet) 17/2/2022

extends Sprite

export(int) var speed : int = 1400
export(int) var damage : int = 25

var velocity : Vector2 = Vector2.RIGHT
var player_rotation : float
var player_owner : int = 0

onready var timer = $Destroy_Timer
onready var hitbox = $Hitbox


puppet var puppet_position : Vector2 setget puppet_position_set
puppet var puppet_velocity : Vector2 = Vector2.ZERO
puppet var puppet_rotation : int = 0

onready var intial_position = global_position


func _ready() -> void:
	add_to_group("Net")
	hitbox.add_to_group("PlayerDamager")
	
	visible = false
	yield(get_tree(), "idle_frame")
	
	if get_tree().has_network_peer():
		if is_network_master():
			velocity = velocity.rotated(player_rotation)
			rotation = player_rotation
			
			rset("puppet_velocity", velocity)
			rset("puppet_rotation", rotation)
			rset("puppet_position", position)
	
	visible = true
	
	timer.connect("timeout", self, "_on_Destroy_Timer_timeout")

func _physics_process(delta : float) -> void:
	if get_tree().has_network_peer():
		if is_network_master():
			global_position += velocity * speed * delta
		else:
			rotation = puppet_rotation
			global_position += puppet_velocity * speed * delta

func puppet_position_set(new_value) -> void:
	puppet_position = new_value
	global_position = puppet_position

sync func destroy() -> void:
	queue_free()

func _on_Destroy_Timer_timeout() -> void:
	if get_tree().has_network_peer():
		if get_tree().is_network_server():
			rpc("destroy")
