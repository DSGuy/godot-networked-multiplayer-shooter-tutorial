# Code written by Oladeji Sanyaolu (NetworkSetup) 17/2/2022

extends Control

var player : PackedScene = load("res://Player.tscn")

var current_spawn_location_instance_number : int = 1
var current_player_for_spawn_location_number = null

onready var multiplayer_config_ui : Control = $Multiplayer_Configure
onready var username_text_edit : LineEdit = $Multiplayer_Configure/Username_Text_Edit
onready var device_ip_address : Label = $UI/Device_IP_Address
onready var create_server_button : Button = $Multiplayer_Configure/Create_Server
onready var join_server_button : Button = $Multiplayer_Configure/Join_Server
onready var start_game_button : Button = $UI/Start_Game


func _ready() -> void:
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	
	create_server_button.connect("pressed", self, "_on_Create_Server_pressed")
	join_server_button.connect("pressed", self, "_on_Join_Server_pressed")
	start_game_button.connect("pressed", self, "_on_Start_Game_pressed")
	device_ip_address.text = Network.ip_address
	
	if get_tree().network_peer != null:
		multiplayer_config_ui.hide()
		
		for player in PersistentNodes.get_children():
			if player.is_in_group("Player"):
				for spawn_location in $Spawn_Locations.get_children():
					if int(spawn_location.name) == current_spawn_location_instance_number and current_player_for_spawn_location_number != player:
						player.rpc("update_position", spawn_location.global_position)
						player.rpc("enable")
						current_spawn_location_instance_number += 1
						current_player_for_spawn_location_number = player
	else:
		start_game_button.hide()

func _process(_delta : float) -> void:
	if get_tree().network_peer != null:
		if get_tree().get_network_connected_peers().size() >= 1 and get_tree().is_network_server():
			start_game_button.show()
		else:
			start_game_button.hide()


func _player_connected(id) -> void:
	print("Player " + str(id) + " has connected")
	
	instance_player(id)

func _player_disconnected(id) -> void:
	print("Player " + str(id) + " has disconnected")
	
	if PersistentNodes.has_node(str(id)):
		PersistentNodes.get_node(str(id)).username_text_instance.queue_free()
		PersistentNodes.get_node(str(id)).queue_free()
	
	if Global.ui != null:
		var prompt = Global.instance_node(load("res://SimplePrompt.tscn"), Global.ui)
		prompt.set_text("Disconnected from server")

func _on_Create_Server_pressed() -> void:
	if username_text_edit.text != "":
		multiplayer_config_ui.hide()
		Network.current_player_username = username_text_edit.text
		Network.create_server()
	
		instance_player(get_tree().get_network_unique_id())

func _on_Join_Server_pressed() -> void:
	if username_text_edit.text != "":
		multiplayer_config_ui.hide()
		username_text_edit.hide()
		
		Global.instance_node(load("res://ServerBrowser.tscn"), self)

func _on_Start_Game_pressed() -> void:
	rpc("switch_to_game")

sync func switch_to_game() -> void:
	get_tree().change_scene("res://Game.tscn")
	
	for child in PersistentNodes.get_children():
		if child.is_in_group("Player"):
			child.update_shoot_mode(true)

func _connected_to_server() -> void:
	yield(get_tree().create_timer(0.1), "timeout")
	instance_player(get_tree().get_network_unique_id())

func instance_player(id) -> void:
	var spawn_pos : Vector2 = get_node("Spawn_Locations/" + str(current_spawn_location_instance_number)).global_position
	var player_instance : Node = Global.instance_node_at_location(player, PersistentNodes, spawn_pos)
	
	player_instance.name = str(id)
	player_instance.set_network_master(id)
	player_instance.username = username_text_edit.text
	
	current_spawn_location_instance_number += 1
