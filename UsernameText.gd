# Code written by Oladeji Sanyaolu (UsernameText) 17/2/2022

extends Node2D

var player_following = null
var text : String = "" setget text_set

onready var label : Label = $Label

func _ready() -> void:
	add_to_group("Net")

func _process(_delta : float) -> void:
	if player_following != null:
		global_position = player_following.global_position

func text_set(new_text) -> void:
	text = new_text
	label.text = text
