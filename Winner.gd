# Code written by Oladeji Sanyaolu (Game/Game_UI/Control/Winner) 17/2/2022

extends Label

onready var winTimer = $Win_Timer

func _ready() -> void:
	winTimer.connect("timeout", self, "_on_Win_Timer_timeout")
	pass

sync func return_to_lobby() -> void:
	get_tree().change_scene("res://NetworkSetup.tscn")

func _on_Win_Timer_timeout() -> void:
	if get_tree().is_network_server():
		rpc("return_to_lobby")
	pass
