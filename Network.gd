# Code written by Oladeji Sanyaolu (Network) 17/2/2022

extends Node

const DEFAULT_PORT : int = 28960
const MAX_CLIENTS : int = 6

var server = null
var client = null

var ip_address : String = ""
var current_player_username : String = ""

var networked_object_name_index : int = 0 setget networked_object_name_index_set
puppet var puppet_networked_object_name_index : int = 0 setget puppet_networked_object_name_index_set

func _ready() -> void:
	randomize()
	
	if OS.get_name() == "Windows":
		ip_address = IP.get_local_addresses()[3]
	elif OS.get_name() == "Android":
		ip_address = IP.get_local_addresses()[0]
	else:
		ip_address = IP.get_local_addresses()[3]
	
	for ip in IP.get_local_addresses():
		if ip.begins_with("192.168.") and not ip.ends_with(".1"):
			ip_address = ip
	
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	get_tree().connect("connection_failed", self, "_connection_failed")

func create_server() -> void:
	server = NetworkedMultiplayerENet.new()
	server.create_server(DEFAULT_PORT, MAX_CLIENTS)
	get_tree().set_network_peer(server)
	Global.instance_node(load("res://ServerAdvertiser.tscn"), get_tree().current_scene)

func join_server() -> void:
	client = NetworkedMultiplayerENet.new()
	client.create_client(ip_address, DEFAULT_PORT)
	get_tree().set_network_peer(client)

func reset_network_connection() -> void:
	if get_tree().has_network_peer():
		get_tree().network_peer = null

func _connected_to_server() -> void:
	print("Successfully connected to the server!")

func _server_disconnected() -> void:
	print("Disconnected from the server!")
	
	for child in PersistentNodes.get_children():
		if child.is_in_group("Net"):
			child.queue_free()
	
	reset_network_connection()
	
	if Global.ui != null:
		var prompt = Global.instance_node(load("res://SimplePrompt.tscn"), Global.ui)
		prompt.set_text("Disconnected from server")

func _connection_failed() -> void:
	for child in PersistentNodes.get_children():
		if child.is_in_group("Net"):
			child.queue_free()
	
	reset_network_connection()

	if Global.ui != null:
		var prompt = Global.instance_node(load("res://SimplePrompt.tscn"), Global.ui)
		prompt.set_text("Connection failed")
	pass

func puppet_networked_object_name_index_set(new_value) -> void:
	networked_object_name_index = new_value

func networked_object_name_index_set(new_value) -> void:
	networked_object_name_index = new_value
	
	if get_tree().is_network_server():
		rset("puppet_networked_object_name_index", networked_object_name_index)
