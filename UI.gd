# Code written by Oladeji Sanyaolu (<ParentNode>/UI) 17/2/2022

extends CanvasLayer

func _ready() -> void:
	Global.ui = self

func _exit_tree() -> void:
	Global.ui = null
