# Code written by Oladeji Sanyaolu (ServerBrowser) 17/2/2022

extends Control

onready var server_listener : Node = $ServerListener
onready var server_ip_text_edit : LineEdit = $Background_Panel/Server_IP_Text_Edit
onready var server_container : VBoxContainer = $Background_Panel/VBoxContainer
onready var manual_setup_button : Button = $Background_Panel/Manual_Setup
onready var join_server_button : Button = $Background_Panel/Server_IP_Text_Edit/Join_Server
onready var go_back_button : Button = $Background_Panel/Go_Back


func _ready() -> void:
	server_ip_text_edit.hide()
	server_listener.connect("new_server", self, "_on_ServerListener_new_server")
	server_listener.connect("remove_server", self, "_on_ServerListener_remove_server")
	manual_setup_button.connect("pressed", self, "_on_Manual_Setup_pressed")
	join_server_button.connect("pressed", self, "_on_Join_Server_pressed")
	go_back_button.connect("pressed", self, "_on_Go_Back_pressed")

func _on_ServerListener_new_server(serverInfo) -> void:
	var server_node = Global.instance_node(load("res://ServerDisplay.tscn"), server_container)
	server_node.text = "%s - %s" % [serverInfo.ip, serverInfo.name]
	server_node.ip_address = str(serverInfo.ip)

func _on_ServerListener_remove_server(serverIP) -> void:
	for serverNode in server_container.get_children():
		if serverNode.is_in_group("ServerDisplay"):
			if serverNode.ip_address == serverIP:
				serverNode.queue_free()
				break
	pass

func _on_Manual_Setup_pressed() -> void:
	if manual_setup_button.text != "Exit Setup":
		server_ip_text_edit.show()
		manual_setup_button.text = "Exit Setup"
		server_container.hide()
		server_ip_text_edit.call_deferred("grab_focus")
	else:
		server_ip_text_edit.text = ""
		server_ip_text_edit.hide()
		manual_setup_button.text = "Manual Setup"
		server_container.show()

func _on_Join_Server_pressed() -> void:
	Network.ip_address = server_ip_text_edit.text
	hide()
	Network.join_server()

func _on_Go_Back_pressed() -> void:
	get_tree().reload_current_scene()
